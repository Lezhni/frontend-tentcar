$ = jQuery;

$(document).on('ready', ready);
$(document).on('scroll', scrolling);
$(window).on('resize', resizing);
$(window).on('load', loaded);

function ready() {

    $('input[name="phone"]').inputmask({
  	    mask: '+7 (999) 999-99-99',
  	    showMaskOnHover: false,
  	    showMaskOnFocus: true,
  	});

    if ($(window).innerWidth() <= 768) {
        var isVertical = false;
        var isVerticalScrolling = false;
    } else {
        var isVertical = true;
        var isVerticalScrolling = true;
    }

    $('#slider').slick({
        autoplay: true,
        autoplaySpeed: 5000,
        vertical: isVertical,
        verticalSwiping : isVerticalScrolling,
        arrows: false,
        dots: true
    });
}

function scrolling() {}

function resizing() {}

function loaded() {

    $('.animate').on('scrollSpy:enter', function() {

       if (!$(this).hasClass('visible')) {
           $(this).addClass('visible');
       }
    });
    $('.animate').scrollSpy();
}


$('.item-desc-more').click(function() {

    $('.item-desc').toggleClass('full-content');
    return false;
});

$('.callback').click(function() {

    var target = $(this).attr('href');

    $.magnificPopup.open({
        items: {
            src: target
        },
        type: 'inline'
    });
    return false;
});

$('.mobile-menu').click(function() {

    $('.mobile-menu, .header-menu').toggleClass('opened');
});

$('.item-gallery a').click(function() {

    var img = $(this).attr('href');
    $('.item-img a[data-lightbox]').attr('href', img);
    $('.item-img a[data-lightbox] img').attr('src', img);
    return false;
});
